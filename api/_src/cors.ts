import { NowResponse, NowRequest } from '@now/node'

export default class Cors {
  public static isRequestMethod(request: NowRequest, method: string) {
    return typeof request.method === 'string' && request.method.toLowerCase() === method
  }

  public static setCors(response: NowResponse) {
    response.setHeader('Access-Control-Allow-Origin', '*')
    response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, POST')
    response.setHeader('Access-Control-Allow-Headers', '*')
  }

  public static sendCors(response: NowResponse) {
    response.writeHead(200)
    response.end()
  }

}
