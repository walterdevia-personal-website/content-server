export interface RawSkill {
  name: string;
  description: string;
  level: 'low' | 'middle' | 'high';
  image: {
    sys: {
      id: string;
    }
  };
  category: 'front-end' | 'back-end' | 'storage' | 'languages' | 'communication';
}

export interface RawProject {
  name: string;
  description: string;
  url: string;
  image: {
    sys: { id: string; };
  };
  type: 'hobby' | 'real';
}

export interface RawMainPhoto {
  photo: {
    sys: { id: string; }
  }
}

export interface ContactLinks {
  skype: string;
  github: string;
  gitlab: string;
  curriculum: string;
  email: string;
}

export type ContenfulResponse = RawSkill | RawProject | RawMainPhoto | About | RawMainPhoto | ContactLinks;

export const SkillCategoriesOrder = ['languages', 'front-end', 'back-end', 'communication', 'storage']

export interface Skill {
  name: string;
  description: string;
  level: 'low' | 'middle' | 'high';
  image: string;
  category: 'front-end' | 'back-end' | 'storage' | 'languages' | 'communication';
}

export interface Project {
  name: string;
  description: string;
  url: string;
  image: string;
  type: 'hobby' | 'real';
}

export interface About {
  inShort: string;
  details: string;
}
