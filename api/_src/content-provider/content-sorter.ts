// Own
import { Project, Skill, SkillCategoriesOrder } from '../types'

export default class ContentSorter {
  public sortProjects(projects: Project[]): Project[] {
    return projects.sort((x, _) => x.type === 'real' ? -1 : 1)
  }

  public sortSkills(skills: Skill[]): Skill[] {
    return skills.sort((a, b) => {
      const aIndex = SkillCategoriesOrder.indexOf(a.category)
      const bIndex = SkillCategoriesOrder.indexOf(b.category)

      if (aIndex < bIndex) {
        return -1
      }

      if (aIndex === bIndex) {
        return 0
      }

      return 1
    })
  }
}
