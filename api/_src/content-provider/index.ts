import { EntryCollection, Asset, createClient } from 'contentful'

// Own
// Types
import { ContenfulResponse, RawProject, Project, RawSkill, Skill, About, RawMainPhoto, ContactLinks } from '../types'
import ContentSorter from './content-sorter'

export default class ContentProvider {
  private content: EntryCollection<ContenfulResponse> | null = null
  private projects: Project[] = []
  private skills: Skill[] = []
  private mainPhoto: string | null = null
  private about: About | null = null
  private contactLinks: ContactLinks | null = null
  private contentSorter = new ContentSorter

  private get contentfulSpaceId(): string {
    return process.env.PERSONAL_WEBSITE_CONTENTFUL_SPACE_ID as string
  }

  private get contentfulAccessToken(): string {
    return process.env.PERSONAL_WEBSITE_CONTENTFUL_TOKEN as string
  }

  public async getContent(): Promise<any> {
    this.content = await this.fetchContent()
    this.abstractContent()

    return {
      projects: this.contentSorter.sortProjects(this.projects),
      skills: this.contentSorter.sortSkills(this.skills),
      mainPhoto: this.mainPhoto,
      about: this.about,
      contactLinks: this.contactLinks
    }
  }

  private async fetchContent(): Promise<EntryCollection<ContenfulResponse>> {
    return createClient({
      accessToken: this.contentfulAccessToken,
      space: this.contentfulSpaceId,
      environment: 'master',
      resolveLinks: true,
    })
    .getEntries()
    .catch(error => {
      console.log('There was an error getting contentful entries. Details...', error)
      return Promise.reject(error + '') as any
    })
  }

  private abstractContent() {
    this.content!.items.forEach(x => {
      switch (x.sys.contentType.sys.id) {
        case 'project':
          this.addProject(x.fields as RawProject)
          break
        case 'skill':
          this.addSkill(x.fields as RawSkill)
          break
        case 'about':
          this.about = x.fields as About
          break
        case 'mainPhoto':
          this.mainPhoto = this.getAssetFromContent((x.fields as RawMainPhoto).photo.sys.id)
          break
        case 'contactLinks':
          this.contactLinks = x.fields as ContactLinks
          break
      }
    })
  }

  private getAssetFromContent(id: string): string {
    const asset = (this.content!.includes.Asset as Asset[]).find(x => x.sys.id === id)
    return asset!.fields.file.url
  }

  private addProject(rawProject: RawProject) {
    this.projects.push({
      name: rawProject.name,
      description: rawProject.description,
      type: rawProject.type,
      url: rawProject.url,
      image: this.getAssetFromContent(rawProject.image.sys.id)
    })
  }

  private addSkill(rawSkill: RawSkill) {
    this.skills.push({
      name: rawSkill.name,
      description: rawSkill.description,
      level: rawSkill.level,
      category: rawSkill.category,
      image: this.getAssetFromContent(rawSkill.image.sys.id)
    })
  }

}
