import { NowResponse, NowRequest } from '@now/node'

// Own
import Cors from './_src/cors'
import ContentProvider from './_src/content-provider'

export default async function(req: NowRequest, res: NowResponse) {
  Cors.setCors(res)

  if (Cors.isRequestMethod(req, 'options')) {
    return Cors.sendCors(res)
  }

  const result = await new ContentProvider().getContent()
    // Avoid promise rejection
    .catch(error => error)

  return res.json(result)
}
